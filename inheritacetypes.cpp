//Demonstrate different types of inheritance in Java.
#include<iostream>
using namespace std;
class Base1
{
    public:
       void display()
       {
        cout<<"successful"<<endl;
       }
};
class Derived1:public virtual Base1{};//single inheritance
class Derived2:public  Derived1{};//multilevel
class Derived3:public virtual Base1{};//mutiple 
class Derived4: public Derived1,public  Derived3{};//hierarichal
int main()
{
    Derived1 obj1;
    obj1.display();
    Derived2 obj2;
    obj2.display();
    Derived3 obj3;
    obj3.display();
    Derived4 obj4;
    obj4.display();
}

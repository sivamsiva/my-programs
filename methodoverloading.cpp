#include <iostream>
using namespace std;
void print(int x) {
  cout << "The integer value is: " << x << std::endl;
}

void print(double x) {
  cout << "The double value is: " << x << std::endl;
}

void print(string x) {
  cout << "The string value is: " << x << std::endl;
}

int main() {
  print(42);          // calls the print(int) function
  print(3.14);        // calls the print(double) function
  print("Hello, C++"); // calls the print(string) function
  return 0;
}

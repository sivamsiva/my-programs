
class Animal {
    protected String name;
 
    protected void eat() {
       System.out.println("Animal is eating");
    }
    private void method1()
    {
      System.out.println("Animal is doing something");
    }
    public void barking()
    {
      System.out.println("Animal is barking");
    }
 }
 
 // Derived class
 class Dog extends Animal {
    protected void bark() {
       System.out.println("Dog is barking");
    }
    public int k=10;
    private int z=5;
 }
 
 // Example usage
 public class Main1 {
    public static void main(String[] args) {
       Dog dog = new Dog();
       dog.name = "Fido";    // Protected member is accessible from derived class
       dog.eat();            // Protected method is accessible from derived class
       dog.bark();           // Derived class method is accessible
       dog.barking(); // public method is accessible
       //dog.method1();// we can't do this
       dog.k=7;   //derived class method is inaccessible when we decalre it as private

    }
 }



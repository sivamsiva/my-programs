public class Main {
    static void myStaticMethod()
    {
        System.out.println("static method wil be executed");
    }
    public void myPublicmethod()
    {
        System.out.println("public method will be executed");
    }
    public static void main(String args[])
    {
        myStaticMethod();//we can call a static method even without creating any object
       // myPublicmethod(); we can't do this
        Main object=new Main();
        object.myPublicmethod();
    }
}

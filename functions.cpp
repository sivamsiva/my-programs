#include<iostream>
using namespace std;
class Box
{   
    public:
          float length,width,height;
          void setValues(float len,float wid,float hei)
          {
                length=len;
                width=wid;
                height=hei;
          }
          void boxArea(float len,float wid,float hei)
          {
              cout<<"the box area is : "<<endl;
              cout<<2*(len*wid+wid*hei+hei*len)<<endl;
          }
          
          void boxVolume(float len,float wid,float hei);
          friend void displayBoxDimensions(Box);
          inline void displayWelcomeMessage()
          {
            cout<<"welcome to the programming in c++"<<endl;
          }

};
void Box::boxVolume(float len,float wid,float hei)
{
    cout<<"scope resolution function executed!"<<endl;
    cout<<len*wid*hei<<endl;
     
}
void displayBoxDimensions(Box b)
{
     cout<<"the length is :" <<b. length<<endl;
     cout<<"the breadth is :"<<b.height<<endl;
     cout<<"the width is :"<<b.width<<endl;

}
int main()
{   float h,w,l;
   
    cout<<"enter the height and breadth and width of the box"<<endl;
    cin>>h>>w>>l;
     Box b;
    b.setValues(h,w,l);
    b.boxArea(h,w,l);
    b.boxVolume(h,w,l);
   b.displayWelcomeMessage();
    displayBoxDimensions(b);
}

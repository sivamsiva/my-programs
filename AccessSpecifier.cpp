/*Write a C++ class 'AccessSpecifierDemo' with the following members:
Member Variables :
1. private int priVar
2. protected int proVar
3. public int pubVar
Member Methods:
1. public void setVar(nt priValue,int proValue, iint pubValue)
2. public void getVar()
Assign values for each member variable(priVar,proVar,pubvar) and using methods(setVar(),getVar()) and disaply them.*/
#include<iostream>
using namespace std;
class AcessSpecifierDemo
{
   private:
        int priVar;
   protected:
        int proVar;
   public:
        int pubVar;
   public:
        void setVar(int priValue,int proValue, int pubValue) 
        {
            priVar=priValue;
            proVar=proValue;
            pubVar=pubValue;
            cout<<"using setter method values successfully set!"<<endl;
        }
        int getpriVar()
        {
           return priVar;
           
           
        }
        int getproVar()
        {
            return proVar;
        }
        int getpubVar()
        {
           return pubVar;
        }

};
int main()
{
    AcessSpecifierDemo obj;
    obj.setVar(90,60,30);
    cout<< "the value of priVar is "<<obj.getpriVar()<<endl;
    cout<< "the value of proVar is "<<obj.getproVar()<<endl;
    cout<< "the value of pubVar is "<<obj.getpubVar()<<endl;
    obj.pubVar=6;

}

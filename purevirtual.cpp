#include<iostream>
using namespace std;
class Animal
{
    public:
      virtual void function()=0;
};
class cow:public Animal{
public:
    void function()
    {
        cout<<"cow is giving us milk"<<endl;
    }
};
class dog:public Animal{
public:
    void function()
    {
        cout<<"dog is giving us faith"<<endl;
    }
};
class donkey:public Animal{
    public:
     int a=10;          //if we do't override the vitual function then this class is also a abstract class
                        // so we can't create onjectios to this class;
};
int main()
{
  cow c;
  c.function();
  dog d;
  d.function();
  
}

import javax.swing.*;
public class MyFrame{
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setTitle("My JFrame");
        frame.setSize(400, 300);
        frame.setVisible(true);
        frame.setResizable(true);
        JLabel label = new JLabel("these are the basic methods of jrame");
        frame.add(label);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}

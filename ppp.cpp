#include<iostream>
using namespace std;
/* 
public,private,protected-----> ACCESS MODIFIERS(when we consider class and object)
public,private,protected-----> VISABULITY MODIFIERS(when we consider inheritance)
*/
class Base
{
    private:
         int a=5;
    protected:
         int b=6;
    public:
         int getvalue()
         {
            return a;
         }
         int setValue(int x)
         {
            a=x;
         }
};
class child1:protected Base{ //all the protected and public members from base calss become protected in child1;
  public:                    //it means only child1 derived class class can use the properties
       void display()
       {
        cout<<this->b;
       }
};
class child2:private Base{//all the protected and public members become private to child2 
    public:
        void display()
        {
            cout<<this->b;
        }
};
class child3:public Base{ // protected methods becomes protected and public methods become public
    public:
        void display()
        {
           cout<<this->b;
        }
};
int main()                 
{
    child1 c;
    child2 c1;
    child3 c2;
    c.display();
    c1.display();
    c2.display();
    

}

